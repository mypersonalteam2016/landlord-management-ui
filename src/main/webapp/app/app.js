'use strict';

/**
 * @ngdoc overview
 * @name sampalmerApp
 * @description
 * # sampalmerApp
 *
 * Main module of the application.
 */
//angular
//  .module('sampalmerApp', [
//    'ngAnimate',
//    'ngCookies',
//    'ngResource',
//    'ngRoute',
//    'ngSanitize',
//    'ngTouch'
//  ])
//  .config(function ($routeProvider) {
//    $routeProvider
//      .when('/', {
//        templateUrl: 'views/main.html',
//        controller: 'MainCtrl',
//        controllerAs: 'main'
//      })
//      .when('/about', {
//        templateUrl: 'views/about.html',
//        controller: 'AboutCtrl',
//        controllerAs: 'about'
//      })
//      .otherwise({
//        redirectTo: '/'
//      });
//  });
angular.module('palmerpropertyrental', [
    'ngRoute','ui.bootstrap','xeditable'
])
    .config(function($routeProvider) {

    $routeProvider.when('/', {
        templateUrl : 'app/views/home.html',
        controller : 'home',
        controllerAs: 'controller'
    }).when('/login', {
        templateUrl : 'app/views/login.html',
        controller : 'navigation',
        controllerAs: 'controller'
    }).when('/signup',  {
        templateUrl : 'app/views/signup.html',
        controller : 'signup',
        controllerAs: 'controller'
    }).when('/complete-registration/:token/:userId', {
        templateUrl : 'app/views/registration.html',
        controller : 'registration',
        controllerAs : 'controller'
    }).when('/tenant', {
        templateUrl : 'app/views/create-tenant.html',
        controller : 'tenant',
        controllerAs : 'controller'
    }).when('/create-property', {
        templateUrl : 'app/views/create-property.html',
        controller : 'property',
        controllerAs : 'controller'
    }).when('/view-properties', {
            templateUrl : 'app/views/view-properties.html',
            controller : 'viewProperty',
            controllerAs : 'controller'
        })
        .otherwise('/');

}).config(['$httpProvider', function($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];
}
]).run(function(editableOptions) {
    editableOptions.theme = 'bs3';
});
