angular.module('palmerpropertyrental')
    .controller('tenant', function($http, $scope, $rootScope) {
        var self = this;

        $http.get('form/createTenant').then(function(response) {
            self.createTenant = {'userType' : 'Tenant', 'landlordId' : $rootScope.landlordId};
            $scope.collection = response.data.response;
        });

        self.createNewTenant = function($scope) {
            $http.post('register', this.createTenant).then(function(data) {
                if(!data.data.success) {
                    self.error = true;
                    self.errorMessage = data.data.message;
                } else {
                    self.error = false;
                    self.created = true;
                }
            });
        }

    });
