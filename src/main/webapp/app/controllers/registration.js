angular.module('palmerpropertyrental')
    .controller('registration', function($http, $routeParams) {
        var token = $routeParams.token,
            userId = $routeParams.userId,
            self = this;

        $http.post('complete-user-registration', {token : token, userId : userId}).then(function(data) {
            self.success = data.data.success;
            self.errorMessage = data.data.message;
        });
    });
