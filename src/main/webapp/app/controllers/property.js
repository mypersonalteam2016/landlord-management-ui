angular.module('palmerpropertyrental')
    .controller('property', function($http, $scope, $rootScope) {
        var self = this;

        $http.get('form/createProperty').then(function(response) {
            self.addProperty = {};
            $scope.collection = response.data.response;
        });

        self.createProperty = function($scope) {
            $http.post('properties', this.addProperty).then(function(data) {
                if(!data.data.success) {
                    self.error = true;
                    self.errorMessage = data.data.message;
                } else {
                    self.error = false;
                    self.created = true;
                }
            });
        }

    })
