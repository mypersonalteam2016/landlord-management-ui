angular.module('palmerpropertyrental')
    .controller('signup', function($http, $scope) {
        var self = this;
        self.signup = {};

        self.register = function() {
            if(self.signup.password !== self.signup.confirmPassword) {
                self.error = true;
                self.errorMessage = 'The passwords don\'t match';
                return;
            }
            if(self.signup.landlordId !== undefined) {
                self.signup.userType = 'Tenant'
            } else {
                self.signup.userType = 'Landlord'
            }
            $http.post('register', self.signup).then(function(data) {
                if(!data.data.success) {
                    self.error = true;
                    self.errorMessage = data.data.message;
                } else {
                    self.error = false;
                    self.created = true;
                }
            });
        }

    });
