angular.module('palmerpropertyrental')
    .controller('viewProperty', function($http, $scope, $uibModal) {
        var self = this;

        this.deleteTenantFromProperty = function(id) {
            console.log(id);

        };

        this.modifyPopup = function(id) {
            $scope.currentModalId = id;
            $http.get('form/createProperty').then(function(response) {
                self.addProperty = {};
                $scope.collection = response.data.response;

                angular.forEach($scope.propertyTenants, function(entry, key) {
                    if(entry.id === id) {
                        $scope.tenants = entry.tenants;
                        self.addProperty.address = entry.address;
                        self.addProperty.rent = entry.rent;
                        self.addProperty.available = entry.available;
                    }
                });

                var modalInstance = $uibModal.open({
                    templateUrl: 'editProperty.html',
                    scope : $scope
                });

                modalInstance.result.then(function () {
                    console.log($scope.currentModalId);
                }, function () {
                    console.log('');
                });

            });


        };

        $scope.toggleAnimation = function () {
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };

        $scope.tenantsDD = [{id : 1, name : 'bob'},{id : 2, name : 'alice'}]


        $scope.showTenants = function(id) {
            var selected = [];
            angular.forEach($scope.propertyTenants, function(propertyTenant) {
                if(propertyTenant.id === id) {
                    angular.forEach(propertyTenant.tenants, function(tenant) {
                        selected.push(tenant.name);
                    })
                }
            });
            return selected.length ? selected.join(', ') : 'Not set';
        };

        $http.get('properties').then(function(response) {
            var properties = response.data.response;
            $scope.propertyTenants = [];

            angular.forEach(properties, function(property) {
                $http.get('user/property/' + property.id).then(function(response) {
                    if(response.data.success) {
                        var users = response.data.response,
                            emails = [],
                            tenants = [];

                        angular.forEach(users, function(user) {
                            emails.push(user.name);
                            tenants.push({id : user.id, name : user.name});
                        });



                        $scope.propertyTenants.push({
                            'id' : property.id,
                            'address' : property.address,
                            'rent' : property.rent,
                            'emails' : emails,
                            'tenants' : tenants
                        });
                    }
                });
            });

        });



    });
