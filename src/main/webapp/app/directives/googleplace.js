angular.module('palmerpropertyrental')
    .directive('googleplace', function($timeout) {
        return {
            require: 'ngModel',
            link: function(scope, element, attrs, model) {
                var lookup = function() {
                    var options = {
                        types: [],
                        componentRestrictions: {country : 'uk'}
                    };
                    scope.gPlace = new google.maps.places.Autocomplete(element[0], options);

                    google.maps.event.addListener(scope.gPlace, 'place_changed', function() {
                        scope.$apply(function() {
                            model.$setViewValue(element.val());
                        });
                    });
                }
                $timeout(lookup, 0);
            }
        };
    });
