angular.module('palmerpropertyrental')
    .directive('inputFields', function($compile) {
        return function(scope, elem, attrs) {
            var obj = angular.fromJson(attrs.inputFields),
                modelName = attrs.modelName,
                html = '<label for="{{obj.id}}">{{obj.label}}</label>',
                el,compiled;
            if(obj.type === 'text' || obj.type === 'checkbox') {
                html += '<input class="form-control" id="{{obj.id}}" type="{{obj.type}}" placeholder="{{obj.placeholder}}"';
            } else if(obj.type === 'select') {
                html += '<select class="form-control" id="{{obj.id}}" placeholder="{{obj.placeholder}}" ';
            }
            html += 'ng-model="' + modelName + '.' + obj.model + '"' + obj.directive +'>';
            el = angular.element(html),
                compiled = $compile(el);
            elem.append(el);
            compiled(scope);
        }
    });
