angular.module('palmerpropertyrental')
    .directive('propertyoptions', function($compile, $http) {
        return function(scope, elem, attrs) {
            var el,compiled;
            $http.get('properties').then(function(data) {
                var properties = data.data.response,html = '';
                angular.forEach(properties, function(property) {
                    html += '<option value="' + property.id + '">' + property.address + '</option>'
                });
                el = angular.element(html),
                    compiled = $compile(el);
                elem.append(el);
                compiled(scope);
            });
        }

    });
