angular.module('palmerpropertyrental')
    .directive('removeTenant', function($http) {
    return function(scope, elt, attrs) {
        scope.removeTenant = function(id) {
            $http.post('user/delete-user-property/' + id, this.addProperty).then(function(data) {
                if(!data.data.success) {
                    self.error = true;
                    self.errorMessage = data.data.message;
                } else {
                    self.error = false;
                    self.created = true;
                }
            });
            elt.remove();
        };
    }
});
