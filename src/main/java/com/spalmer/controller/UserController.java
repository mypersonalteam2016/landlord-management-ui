package com.spalmer.controller;

import com.spalmer.service.RestService;
import com.spalmer.user.model.response.ResponseDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Map;

@RestController
public class UserController {

	private RestService restService;

	@Autowired
	public UserController(final RestService restService) {
		this.restService = restService;
	}

	@RequestMapping(value = "/register", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	ResponseDTO signup(@RequestBody Map<String, String> map) {
		return restService.post("/register", map);
	}

	@RequestMapping(value = "/complete-user-registration", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody
	ResponseDTO completeRegistration(@RequestBody Map<String, String> map) {
		return restService.post("/complete-user-registration", map);
	}

}
