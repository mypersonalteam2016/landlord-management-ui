package com.spalmer.configuration;

import com.spalmer.service.RestService;
import com.spalmer.user.model.response.ResponseDTO;
import com.spalmer.user.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

@Component
public class CustomUserDetailsService implements UserDetailsService {

	private RestService restService;

	@Autowired
	public CustomUserDetailsService(final RestService restService) {
		this.restService = restService;
	}

	@Override
	public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
		try {
			String encodedUsername = URLEncoder.encode(s, "UTF-8");
			return restService.getUserResponse(encodedUsername);
		} catch (UnsupportedEncodingException e) {
			return null;
		}

	}
}
