package com.spalmer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@SpringBootApplication
@EnableZuulProxy
public class LandlordManagementUI {

	public static void main(String[] args) {
		SpringApplication.run(LandlordManagementUI.class, args);
	}
}
