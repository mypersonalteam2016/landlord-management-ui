package com.spalmer.service;

import com.spalmer.user.model.response.ResponseDTO;
import com.spalmer.user.model.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RestServiceImpl implements RestService {


	@Value("${zuul.routes.user.url}")
	private String url;


	private RestTemplate restTemplate;
	public static final ParameterizedTypeReference<ResponseDTO> REFERENCE = new ParameterizedTypeReference<ResponseDTO>() {
	};
	public static final ParameterizedTypeReference<ResponseDTO<User>> USER_REFERENCE = new ParameterizedTypeReference<ResponseDTO<User>>() {
	};

	@Autowired
	public RestServiceImpl(final RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}

	@Override
	public <T> ResponseDTO<T> get(String url) {
		return exchange(url, HttpMethod.GET, REFERENCE, null);
	}

	@Override
	public ResponseDTO post(String urlString, Object postData) {
		return exchange(url + urlString, HttpMethod.POST, REFERENCE, new HttpEntity<>(postData));
	}

	private ResponseDTO exchange(String url, HttpMethod method, ParameterizedTypeReference<ResponseDTO> reference, HttpEntity entity) {

		ResponseEntity<ResponseDTO> responseEntity =  restTemplate.exchange(url, method,entity,reference);
		return responseEntity.getBody();
	}

	@Override
	public User getUserResponse(String username) {
		ResponseDTO<User> userResponse = restTemplate
				.exchange(url + "/username/" + username, HttpMethod.GET, null, USER_REFERENCE).getBody();

		return userResponse.getResponse();
	}
}
