package com.spalmer.service;

import com.spalmer.user.model.response.ResponseDTO;
import com.spalmer.user.model.user.User;

public interface RestService {

	<T> ResponseDTO<T> get(String url);
	<T> ResponseDTO<T> post(String url, Object postData);
	User getUserResponse(String url);
}
